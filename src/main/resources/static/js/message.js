function bodyOnLoad() {
    let message = sessionStorage.getItem('message');
    sessionStorage.clear();
    let h = document.getElementById("error-message");
    h.textContent = message;
}