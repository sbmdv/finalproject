function block() {
    let name = document.querySelector('#name').value.trim();
    let query = "/admin/blocked?name=" + name + "&action=blocked";
    if (name) {
        fetch(query)
            .then(function(response) {
                if (!response.ok) {
                    let message = "Извините, что то пошло не так";
                    sessionStorage.setItem('message', message);
                    location.href = "error.html";
                    return Promise.reject(new Error(
                        'Response failed: ' + response.status + ' (' + response.statusText + ')'
                    ));
                }
                return response.json();
            })
            .then((data) => {
                if (data.ACT == "YES") {
                    let h = document.getElementById("message-view");
                    h.textContent = "Пользователь заблокирован";
                    document.querySelector('#admin-page').classList.add('hidden');
                    document.querySelector('#admin-message').classList.remove('hidden');
                } else {
                    let h = document.getElementById("message-view");
                    h.textContent = "Пользователь заблокирован ранее";
                    document.querySelector('#admin-page').classList.add('hidden');
                    document.querySelector('#admin-message').classList.remove('hidden');
                }
            });
    }
}

function unblock() {
    let name = document.querySelector('#name').value.trim();
    let query = "/admin/blocked?name=" + name + "&action=unblocked";
    if (name) {
        fetch(query)
            .then(function(response) {
                if (!response.ok) {
                    let message = "Извините, что то пошло не так";
                    sessionStorage.setItem('message', message);
                    location.href = "error.html";
                    return Promise.reject(new Error(
                        'Response failed: ' + response.status + ' (' + response.statusText + ')'
                    ));
                }
                return response.json();
            })
            .then((data) => {
                if (data.ACT == "YES") {
                    let h = document.getElementById("message-view");
                    h.textContent = "Пользователь разблокирован";
                    document.querySelector('#admin-page').classList.add('hidden');
                    document.querySelector('#admin-message').classList.remove('hidden');

                } else {
                    let h = document.getElementById("message-view");
                    h.textContent = "Такой пользователь ране не блокировался";
                    document.querySelector('#admin-page').classList.add('hidden');
                    document.querySelector('#admin-message').classList.remove('hidden');
                }
            });
    }
}