'use strict';

var usernamePage = document.querySelector('#username-page');
var usernamePageLogin = document.querySelector('#username-page-login');
var usernamePageRegister = document.querySelector('#username-page-register');
var userPageRules = document.querySelector('#chat-page-rules');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var usernameRegister = document.querySelector('#usernameRegister');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var userArea = document.querySelector('#userArea');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;
var room = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function loginClick() {
    room = document.querySelector('#value-select').value;
    usernamePage.classList.add('hidden');
    usernamePageLogin.classList.remove('hidden');
    event.preventDefault();
}

function rulesClick() {
    usernamePage.classList.add('hidden');
    userPageRules.classList.remove('hidden');
    event.preventDefault();
}

function regClick() {
    userPageRules.classList.add('hidden');
    usernamePageRegister.classList.remove('hidden');
    event.preventDefault();
}

function registerUser(event) {
    let uname = document.querySelector('#uname').value.trim(),
        nickname = document.querySelector('#nickname').value.trim(),
        upassword = document.querySelector('#upassword').value.trim(),
        rpassword = document.querySelector('#rpassword').value.trim();

    if (upassword) {
        if (upassword === rpassword) {
            if (uname && nickname) {
                const data = {
                    name: uname,
                    nick: nickname,
                    password: upassword
                };
                fetch('/chat.registryUser', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8'
                    },
                    body: JSON.stringify(data)
                }).then(function(response) {

                    if (!response.ok) {
                        let message = "Извините такой логин уже есть";
                        sessionStorage.setItem('message', message);
                        location.href = "error.html";
                        return Promise.reject(new Error(
                            'Response failed: ' + response.status + ' (' + response.statusText + ')'
                        ));
                    }
                    usernamePageRegister.classList.add('hidden');
                    usernamePage.classList.remove('hidden');
                });
            } else {
                let message = "Пожалуйста заполните Ваши имя и ник";
                sessionStorage.setItem('message', message);
                location.href = "error.html";
            }

        } else {
            let message = "Извините, пароли не совпадают";
            sessionStorage.setItem('message', message);
            location.href = "error.html";
        }
    }

    event.preventDefault();
}

function connect(event) {
    username = document.querySelector('#name').value.trim();
    let password = document.querySelector('#password').value.trim();

    if (username && password) {
        const data = {
            name: username,
            nick: username,
            password: password
        };

        fetch('/' + room + '/chat.loginUser', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify(data)
            }).then(function(response) {
                if (!response.ok) {
                    let message = "Извините, что то пошло не так";
                    sessionStorage.setItem('message', message);
                    location.href = "error.html";
                    return Promise.reject(new Error(
                        'Response failed: ' + response.status + ' (' + response.statusText + ')'
                    ));
                }
                return response.json();
            })
            .then((data) => {
                if (data.IN == "ADMIN") {
                    location.href = "/admin";
                } else if (data.IN == "OK") {
                    sessionStorage.setItem('nickname', username);
                    document.getElementById("room").innerHTML = room;
                    usernamePageLogin.classList.add('hidden');
                    chatPage.classList.remove('hidden');
                    var socket = new SockJS('/ws');
                    stompClient = Stomp.over(socket);
                    stompClient.connect({}, onConnected, onError);
                } else if (data.IN == "BLOCK") {
                    let message = "Извините, пользователь заблокирован";
                    sessionStorage.setItem('message', message);
                    location.href = "error.html";
                } else if (data.IN == "FAIL") {
                    let message = "Извините, не верный логин или пароль";
                    sessionStorage.setItem('message', message);
                    location.href = "error.html";
                } else if (data.IN == "CLASH") {
                    let message = "Извините, такой пользователь авторизован";
                    sessionStorage.setItem('message', message);
                    location.href = "error.html";
                } else {
                    let message = "Извините, ошибка ответа сервера";
                    sessionStorage.setItem('message', message);
                    location.href = "error.html";
                }
            });
    }
    event.preventDefault();
}

function onConnected() {
    stompClient.subscribe('/topic/' + room + '/public', onMessageReceived);
    stompClient.send('/app/' + room + '/chat.addUser', {},
        JSON.stringify({ sender: username, room: room, type: 'JOIN' })
    )

    connectingElement.classList.add('hidden');
    loadOnlineUsers();
    loadHistoryUsers();
}

function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}


function sendMessage(event) {
    var messageContent = messageInput.value.trim();

    if (messageContent && stompClient) {
        let current_date = new Date().toLocaleDateString();
        let current_time = new Date().toLocaleTimeString().slice(0, -3);
        let current_timestamp = current_date + ";" + current_time;
        var chatMessage = {
            sender: username,
            content: messageInput.value,
            room: room,
            timestamp: current_timestamp,
            type: 'CHAT'
        };
        console.log("Отправляем сообщение");
        stompClient.send('/app/' + room + '/chat.sendMessage', {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}


function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);
    var areaUser = false;
    var messageElement = document.createElement('li');
    var nickname = sessionStorage.getItem('nickname');

    if (message.type === 'JOIN') {
        areaUser = true;
        let idAttribute = 'id_' + message.sender;
        messageElement.setAttribute('id', idAttribute);


        if (nickname === message.sender) {
            messageElement.classList.add('event-owner');
        } else {
            messageElement.classList.add('event-user-online');
        }

        message.content = message.sender;

    } else if (message.type === 'LEAVE') {
        let idOut = 'id_' + message.sender;
        let removeElement = document.getElementById(idOut);
        if (removeElement !== null) {
            removeElement.remove();
        }
        messageElement.classList.add('event-message');
        message.content = message.sender + ' покинул нас!';
    } else {
        let temp = message.timestamp;
        let timestamp = temp.split(';');
        messageElement.classList.add('chat-message');

        let dateElement = document.createElement('p');
        let dateText = document.createTextNode(timestamp[0]);
        dateElement.appendChild(dateText);
        dateElement.style.textAlign = 'center';
        dateElement.style.fontSize = 'x-small';
        messageElement.appendChild(dateElement);

        let avatarElement = document.createElement('i');
        let avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        let usernameElement = document.createElement('span');
        let usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);

        let timeElement = document.createElement('span');
        let timeText = document.createTextNode(timestamp[1]);
        timeElement.appendChild(timeText);
        timeElement.classList.add('message-time');
        messageElement.appendChild(timeElement);

    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);
    messageElement.appendChild(textElement);

    if (areaUser) {
        userArea.appendChild(messageElement);
        userArea.scrollTop = userArea.scrollHeight;

        let elementLi = document.createElement('li');
        elementLi.classList.add('event-message');

        let textElementLi = document.createElement('p');
        let messageTextLi = document.createTextNode("Приветствуем тебя " +
            messageElement.textContent +
            "!");
        textElementLi.appendChild(messageTextLi);
        elementLi.appendChild(textElementLi);

        messageArea.appendChild(elementLi);
        messageArea.scrollTop = messageArea.scrollHeight;
    } else {
        messageArea.appendChild(messageElement);
        messageArea.scrollTop = messageArea.scrollHeight;
    }
}

function logout() {
    sessionStorage.clear();
    document.location.href = "/";

}

function loadOnlineUsers() {
    let nick = sessionStorage.getItem('nickname');
    let query = "/" + room + "/chat.onlineUsers";
    fetch(query)
        .then(function(response) {
            if (!response.ok) {
                let message = "Извините, что то пошло не так";
                sessionStorage.setItem('message', message);
                location.href = "error.html";
                return Promise.reject(new Error(
                    'Response failed: ' + response.status + ' (' + response.statusText + ')'
                ));
            }
            return response.json();
        })
        .then((data) => {
            for (let i = 0; i < data.length; i++) {
                let arrayArg = data[i];
                if (nick != arrayArg) {

                    let elementLi = document.createElement('li');
                    let idAttribute = 'id_' + arrayArg;
                    elementLi.setAttribute('id', idAttribute);
                    elementLi.classList.add('event-user-online');

                    let textElementLi = document.createElement('p');
                    let messageTextLi = document.createTextNode(arrayArg);
                    textElementLi.appendChild(messageTextLi);
                    elementLi.appendChild(textElementLi);

                    userArea.appendChild(elementLi);
                    userArea.scrollTop = messageArea.scrollHeight;
                }
            }
        });
}

function loadHistoryUsers() {
    let query = "/" + room + "/chat.history";
    fetch(query)
        .then(function(response) {
            if (!response.ok) {
                let message = "Извините, что то пошло не так";
                sessionStorage.setItem('message', message);
                location.href = "error.html";
                return Promise.reject(new Error(
                    'Response failed: ' + response.status + ' (' + response.statusText + ')'
                ));
            }
            return response.json();
        })
        .then((data) => {
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    let sender = data[i].sender;
                    let content = data[i].content;
                    let temp = data[i].timestamp;
                    let timestamp = temp.split(';');
                    let elementHistory = document.createElement('li');
                    elementHistory.classList.add('chat-message');

                    let dateElement = document.createElement('p');
                    let dateText = document.createTextNode(timestamp[0]);
                    dateElement.appendChild(dateText);
                    dateElement.style.textAlign = 'center';
                    dateElement.style.fontSize = 'x-small';
                    elementHistory.appendChild(dateElement);

                    let avatarElement = document.createElement('i');
                    let avatarText = document.createTextNode(sender[0]);
                    avatarElement.appendChild(avatarText);
                    avatarElement.style['background-color'] = getAvatarColor(sender);
                    elementHistory.appendChild(avatarElement);

                    let usernameElement = document.createElement('span');
                    let usernameText = document.createTextNode(sender);
                    usernameElement.appendChild(usernameText);
                    elementHistory.appendChild(usernameElement);

                    let timeElement = document.createElement('span');
                    let timeText = document.createTextNode(timestamp[1]);
                    timeElement.appendChild(timeText);
                    timeElement.classList.add('message-time');
                    elementHistory.appendChild(timeElement);

                    let textElement = document.createElement('p');
                    let messageText = document.createTextNode(content);
                    textElement.appendChild(messageText);
                    elementHistory.appendChild(textElement);

                    messageArea.appendChild(elementHistory);
                    messageArea.scrollTop = messageArea.scrollHeight;
                }
            }
        });

}

function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}

usernameForm.addEventListener('submit', connect, true)
usernameRegister.addEventListener('submit', registerUser, true)
messageForm.addEventListener('submit', sendMessage, true)