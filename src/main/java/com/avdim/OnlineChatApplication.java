package com.avdim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Класс OnlineChatApplication основной класс запуска програамм.
 */
@SpringBootApplication
public class OnlineChatApplication {

    /**
     * Метод main запускает программу на выполнение.
     *
     * @param args - список параметров передаваемые в программу при запуске.
     */
    public static void main(String[] args) {
        SpringApplication.run(OnlineChatApplication.class, args);
    }
}
