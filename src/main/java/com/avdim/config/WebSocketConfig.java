package com.avdim.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Класс WebSocketConfig содержит данные для начальной конфигурации приложения
 * является классом имплементирующим методы интерфейса WebSocketMessageBrokerConfigurer.
 *
 * @author Медянцев Денис
 * @version 1.0
 */
@Configuration
@ComponentScan("com.avdim")
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    /**
     * Это метод настраивает конечную точку подключения для клиентов WebSockets с
     * запасным вариантом для SockJS на случай, если клиент (старый браузер)
     * изначально не поддерживает WebSockets
     *
     * @param registry - контракт на регистрацию STOMP через конечные точки WebSocket
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").withSockJS();
    }

    /**
     * Это метод определяет настройки обработки сообщений с помощью простых
     * протоколов обмена сообщениями STOMP от клиентов WebSocket.
     *
     * @param registry - данные для настройки параметров брокера сообщений.
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app");
        registry.enableSimpleBroker("/topic");
    }
}
